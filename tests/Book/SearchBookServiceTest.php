<?php

namespace App\Tests\Book;

use App\Assembler\BookAssembler;
use App\Entity\Book;
use App\Service\BooksApi\GoogleBooksService;
use Google\Service\Books\Volume;
use Google\Service\Books\VolumeVolumeInfo;
use Google\Service\Books\VolumeVolumeInfoImageLinks;
use PHPUnit\Framework\TestCase;
use ReflectionClass;
use ReflectionException;
use Prophecy\PhpUnit\ProphecyTrait;

class SearchBookServiceTest extends TestCase
{
    use ProphecyTrait;

    protected Volume $volume;
    protected Book $expectedBook;

    protected function setUp(): void
    {
        $imgLinks = new VolumeVolumeInfoImageLinks();
        $imgLinks->setThumbnail('Test img');

        $this->volume = new Volume();
        $volumeInfo = new VolumeVolumeInfo();
        $volumeInfo->setTitle('Test title');
        $volumeInfo->setPublisher('Test publisher');
        $volumeInfo->setPageCount(23);
        $volumeInfo->setAuthors(['Author 1', 'Author 2']);
        $volumeInfo->setImageLinks($imgLinks);
        $volumeInfo->setDescription('Test description');
        $this->volume->setVolumeInfo($volumeInfo);
        $this->volume->setId('zer234');

        $this->expectedBook = new Book();
        $this->expectedBook->setTitle('Test title')
            ->setPublisher('Test publisher')
            ->setPageCount(23)
            ->setAuthor('Author 1, Author 2')
            ->setResume('Test description')
            ->setCover('Test img')
            ->setApiItemId('zer234')
        ;
    }

    public function testwriteObjectFromGoogleBookVolume()
    {
        $bookAssembler = new BookAssembler();

        $book = $bookAssembler->writeObjectFromGoogleBookVolume($this->volume);

        $this->assertEquals($this->expectedBook, $book);
    }

    /**
     * @throws ReflectionException
     */
    public function testGoogleBooksVolumesToArray()
    {
        $bookAssemblerMock = $this->prophesize(BookAssembler::class);
        $bookAssemblerMock->writeObjectFromGoogleBookVolume($this->volume)->willReturn($this->expectedBook);

        $googleBooksService = new GoogleBooksService('dummyKey', $bookAssemblerMock->reveal());

        $method = self::getMethod('googleBooksVolumesToBooks');
        $books = $method->invokeArgs($googleBooksService, [[$this->volume]]);

        $this->assertEquals([$this->expectedBook], $books);
    }

    /**
     * @throws ReflectionException
     */
    protected static function getMethod($name): \ReflectionMethod
    {
        $class = new ReflectionClass('App\Service\BooksApi\GoogleBooksService');
        $method = $class->getMethod($name);
        $method->setAccessible(true);
        return $method;
    }
}
