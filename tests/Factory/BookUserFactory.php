<?php

namespace App\Tests\Factory;

use App\Entity\BookUser;
use App\Repository\BookUserRepository;
use Zenstruck\Foundry\RepositoryProxy;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;

/**
 * @extends ModelFactory<BookUser>
 *
 * @method static BookUser|Proxy createOne(array $attributes = [])
 * @method static BookUser[]|Proxy[] createMany(int $number, array|callable $attributes = [])
 * @method static BookUser|Proxy find(object|array|mixed $criteria)
 * @method static BookUser|Proxy findOrCreate(array $attributes)
 * @method static BookUser|Proxy first(string $sortedField = 'id')
 * @method static BookUser|Proxy last(string $sortedField = 'id')
 * @method static BookUser|Proxy random(array $attributes = [])
 * @method static BookUser|Proxy randomOrCreate(array $attributes = [])
 * @method static BookUser[]|Proxy[] all()
 * @method static BookUser[]|Proxy[] findBy(array $attributes)
 * @method static BookUser[]|Proxy[] randomSet(int $number, array $attributes = [])
 * @method static BookUser[]|Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @method static BookUserRepository|RepositoryProxy repository()
 * @method BookUser|Proxy create(array|callable $attributes = [])
 */
final class BookUserFactory extends ModelFactory
{
    public function __construct()
    {
        parent::__construct();

        // TODO inject services if required (https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#factories-as-services)
    }

    protected function getDefaults(): array
    {
        return [
            // TODO add your default values here (https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#model-factories)
            'created' => new \DateTime('now'), // TODO add DATETIME ORM type manually
            'isFavorite' => false,
        ];
    }

    protected function initialize(): self
    {
        // see https://symfony.com/bundles/ZenstruckFoundryBundle/current/index.html#initialization
        return $this
            // ->afterInstantiate(function(BookUser $bookUser): void {})
        ;
    }

    protected static function getClass(): string
    {
        return BookUser::class;
    }
}
