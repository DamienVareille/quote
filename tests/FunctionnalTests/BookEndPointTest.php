<?php

namespace App\Tests\FunctionnalTests;

use App\Entity\Book;
use App\Tests\Factory\BookUserFactory;
use App\Repository\UserRepository;
use App\Service\BooksApi\BooksApi;
use App\Tests\Factory;
use App\Tests\Factory\BookFactory;
use App\Tests\Factory\UserFactory;
use Symfony\Component\HttpFoundation\Response;
use Zenstruck\Foundry\Test\Factories;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class BookEndPointTest extends WebTestCase
{
    use Factories;

    private $connectedUser;
    private $client;

    public function setUp(): void
    {
        $user = UserFactory::new()
            ->create()
        ;
        $userRepository = static::getContainer()->get(UserRepository::class);
        $this->connectedUser = $userRepository->findOneBy(['mail' => $user->getMail()]);

        static::ensureKernelShutdown();
        $this->client = static::createClient();
        $this->client->loginUser($this->connectedUser);
    }

    public function testBookAlreadyExistsInUserLibrary(): void
    {
        $bookUsers = BookUserFactory::new()->create(function () {
            return ['user' => UserFactory::new(),'book' => BookFactory::new(['apiItemId' => '123'])];
        });

        $user = $bookUsers->getUser();

        $userRepository = static::getContainer()->get(UserRepository::class);
        $userObject = $userRepository->findOneBy(['mail' => $user->getMail()]);

        $this->client->loginUser($userObject);
        $this->client->request('GET', '/book/find', ['apiItemId' => '123']);

        $response = $this->client->getResponse();

        $this->assertResponseIsSuccessful();
        $this->assertSame(200, $response->getStatusCode());
        $this->assertEquals('{"inLibrary":true}', $response->getContent());
    }

    public function testBookNotExistsInUserLibrary(): void
    {
        $this->client->request('GET', '/book/find', ['apiItemId' => '123']);

        $response = $this->client->getResponse();

        $this->assertResponseIsSuccessful();
        $this->assertSame(200, $response->getStatusCode());
        $this->assertEquals('{"inLibrary":false}', $response->getContent());
    }

    public function testBookResumeFetchByApiReturn200Code(): void
    {
        $this->client->catchExceptions(false);

        $this->client->request('GET', '/book/resume', ['id' => 'zTVOewAACAAJ']);

        $response = $this->client->getResponse();

        $this->assertResponseIsSuccessful();
        $this->assertSame(200, $response->getStatusCode());
    }

    public function testBookSearchFetchByApiReturn200Code(): void
    {
        $this->client->catchExceptions(false);

        $this->client->request('GET', '/book/search', ['q' => 'Le pouvoir rhétorique']);

        $response = $this->client->getResponse();

        $this->assertResponseIsSuccessful();
        $this->assertSame(200, $response->getStatusCode());
    }

    public function testBookUserDeletion(): void
    {
        $bookUsers = BookUserFactory::new()->create(function () {
            return ['user' => $this->connectedUser,'book' => BookFactory::new(['apiItemId' => '123'])];
        });

        $this->client->request('DELETE', '/book/delete/book-user/' . $bookUsers->getBook()->getID());

        $response = $this->client->getResponse();

        $this->assertResponseIsSuccessful();
        $this->assertSame(Response::HTTP_NO_CONTENT, $response->getStatusCode());
    }

    public function testCurrentReadingsReturn200(): void
    {
        $this->client->request('GET', '/book/current-readings');

        $response = $this->client->getResponse();

        $this->assertResponseIsSuccessful();
        $this->assertSame(Response::HTTP_OK, $response->getStatusCode());
    }
}