<?php

namespace App\Tests\FunctionnalTests;

use App\Repository\UserRepository;
use App\Tests\Factory\BookFactory;
use App\Tests\Factory\BookUserFactory;
use App\Tests\Factory\UserFactory;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Zenstruck\Foundry\Test\Factories;

class HomeEndPointTest extends WebTestCase
{
    use Factories;

    private $connectedUser;
    private $client;

    public function setUp(): void
    {
        $user = UserFactory::new()
            ->create()
        ;
        $userRepository = static::getContainer()->get(UserRepository::class);
        $this->connectedUser = $userRepository->findOneBy(['mail' => $user->getMail()]);

        static::ensureKernelShutdown();
        $this->client = static::createClient();
        $this->client->loginUser($this->connectedUser);
    }

    public function testHomePageReturn200(): void
    {
        $this->client->loginUser($this->connectedUser);
        $this->client->request('GET', '/');

        $response = $this->client->getResponse();

        $this->assertResponseIsSuccessful();
        $this->assertSame(200, $response->getStatusCode());
    }

}