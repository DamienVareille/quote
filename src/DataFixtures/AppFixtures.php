<?php

namespace App\DataFixtures;

use App\Tests\Factory\BookFactory;
use App\Tests\Factory\BookUserFactory;
use App\Tests\Factory\UserFactory;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        UserFactory::new(['mail' => 'test@test.fr'])
            ->create();

        UserFactory::createMany(2);

        BookFactory::createMany(20);

        BookUserFactory::createMany(15, function () {
            return [
                'book' => BookFactory::random(),
                'user' => UserFactory::findBy(['mail' => 'test@test.fr'])[0],
            ];
        });
    }
}
