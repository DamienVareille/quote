<?php

namespace App\EventListener\User;

use App\Entity\User;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

final class UserBlockedNotifier
{
    private LoggerInterface $logger;
    private RequestStack $requestStack;
    private TokenStorageInterface $tokenStorage;

    public function __construct(LoggerInterface $logger, RequestStack $requestStack, TokenStorageInterface $tokenStorage)
    {
        $this->logger = $logger;
        $this->requestStack = $requestStack;
        $this->tokenStorage = $tokenStorage;
    }

    public function postUpdate(User $user): void
    {
        $this->logger->info('User #' . $user->getId() . ' has been updated.');
        if ($user->isBlocked()) {
            $this->tokenStorage->setToken();
            $this->requestStack->getSession()->invalidate();
            $user->eraseCredentials();
            $this->logger->info('User #' . $user->getId() . ' has been blocked.');
        }
    }
}
