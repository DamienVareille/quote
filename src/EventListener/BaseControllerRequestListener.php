<?php

namespace App\EventListener;

use App\Entity\User;
use App\Repository\BookRepository;
use Psr\Cache\InvalidArgumentException;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\Cache\ItemInterface;
use Symfony\Contracts\Cache\TagAwareCacheInterface;

final class BaseControllerRequestListener
{
    private TagAwareCacheInterface $pool;
    private BookRepository $bookRepository;
    private User $user;
    private Security $security;

    public function __construct(TagAwareCacheInterface $pool, BookRepository $bookRepository, Security $security)
    {
        $this->pool = $pool;
        $this->bookRepository = $bookRepository;
        $this->security = $security;
    }

    /**
     * @throws InvalidArgumentException
     */
    public function onKernelRequest(RequestEvent $event)
    {
        // Avoid calling on profiler request
        $route = $event->getRequest()->attributes->get('_route');
        if (null === $route || str_starts_with($route, '_')) {
            return;
        }

        /** @var User $user */
        $user = $this->security->getUser();

        if (null === $user) {
            return;
        }

        $this->user = $user;

        $value = $this->pool->get('favorites_books_cache' . $user->getId(), function (ItemInterface $item) {
            $item->expiresAfter(900);

            return $this->bookRepository->findLastConsultedBooks($this->user, 3);
        });

        $event->getRequest()->query->set('cached_favorite_books', $value);
    }
}
