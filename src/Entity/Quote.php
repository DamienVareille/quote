<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\QuoteRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use JetBrains\PhpStorm\Pure;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: QuoteRepository::class)]
#[Gedmo\SoftDeleteable(fieldName: 'deletedAt', timeAware: false, hardDelete: true)]
#[ApiResource(
    denormalizationContext: ['groups' => ['quote:write']],
    normalizationContext: ['groups' => ['quote:read']],
)]
class Quote
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id = null;

    #[ORM\Column(type: 'text')]
    #[Groups(["quote:write", "quote:read"])]
    private ?string $content;

    #[ORM\ManyToOne(targetEntity: Book::class, inversedBy: 'quotes')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(["quote:write", "quote:read"])]
    private Book $book;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'quotes')]
    #[ORM\JoinColumn(nullable: false)]
    private User $author;

    #[ORM\Column(type: 'integer')]
    #[Groups(["quote:write", "quote:read"])]
    private int $page;

    #[ORM\ManyToMany(targetEntity: 'App\Entity\User', inversedBy: 'likedQuotes')]
    #[ORM\JoinTable(name: 'liked_quotes')]
    private Collection $usersWhoLiked;

    /**
     * @var \DateTime
     */
    #[Gedmo\Timestampable(on: 'create')]
    #[ORM\Column(name: 'created', type: Types::DATETIME_MUTABLE)]
    private $created;

    /**
     * @var \DateTime
     */
    #[ORM\Column(name: 'updated', type: Types::DATETIME_MUTABLE)]
    #[Gedmo\Timestampable]
    private $updated;

    #[ORM\Column(name: 'deletedAt', type: Types::DATETIME_MUTABLE, nullable: true)]
    private \DateTime $deletedAt;

    #[Pure] public function __construct()
    {
        $this->usersWhoLiked = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getBook(): Book
    {
        return $this->book;
    }

    public function setBook(Book $book): self
    {
        $this->book = $book;

        return $this;
    }

    public function getAuthor(): User
    {
        return $this->author;
    }

    public function setAuthor(User $author): self
    {
        $this->author = $author;

        return $this;
    }

    public function getPage(): int
    {
        return $this->page;
    }

    public function setPage(int $page): self
    {
        $this->page = $page;

        return $this;
    }

    public function getUsersWhoLiked(): Collection
    {
        return $this->usersWhoLiked;
    }

    public function setUsersWhoLiked(Collection $usersWhoLiked): self
    {
        $this->usersWhoLiked = $usersWhoLiked;

        return $this;
    }

    public function addUserWhoLiked(User $user): self
    {
        if (!$this->usersWhoLiked->contains($user)) {
            $this->usersWhoLiked->add($user);
            $user->addLikedQuote($this);
        }

        return $this;
    }

    public function removeUserWhoLiked(User $user): self
    {
        if ($this->usersWhoLiked->contains($user)) {
            $this->usersWhoLiked->removeElement($user);
            $user->removeLikedQuote($this);
        }

        return $this;
    }

    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    public function setCreated(\DateTime $created): self
    {
        $this->created = $created;

        return $this;
    }

    public function getUpdated(): \DateTime
    {
        return $this->updated;
    }

    public function setUpdated(\DateTime $updated): self
    {
        $this->updated = $updated;

        return $this;
    }

    public function getDeletedAt(): \DateTime
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(\DateTime $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }
}
