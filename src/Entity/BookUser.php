<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\BookUserRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use phpDocumentor\Reflection\Types\Boolean;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: BookUserRepository::class)]
#[ApiResource(
    collectionOperations: [
        'get' => ['method' => 'get'],
    ],
    itemOperations: [
        'get' => ['method' => 'get'],
    ],
    denormalizationContext: ['groups' => ['book-user:write']],
    normalizationContext: ['groups' => ['book-user:read']],
)]
class BookUser
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\ManyToOne(targetEntity: Book::class, cascade: ['persist'], inversedBy: 'bookUsers')]
    #[Groups("book-user:read")]
    private Book $book;

    #[ORM\ManyToOne(targetEntity: User::class, cascade: ['persist'], inversedBy: 'bookUsers')]
    #[Groups("book-user:read")]
    private User $user;

    #[Gedmo\Timestampable(on: 'create')]
    #[ORM\Column(name: 'created', type: Types::DATETIME_MUTABLE)]
    #[Groups("book-user:read")]
    private \DateTime $created;

    #[ORM\Column(type: 'boolean')]
    #[Groups("book-user:read")]
    private bool $isFavorite = false;

    #[ORM\Column(type: 'datetime', nullable: true)]
    private ?\DateTimeInterface $lastConsulted;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBook(): Book
    {
        return $this->book;
    }

    public function setBook(Book $book): self
    {
        $this->book = $book;

        return $this;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    public function setCreated(\DateTime $created): self
    {
        $this->created = $created;

        return $this;
    }

    public function isFavorite(): ?bool
    {
        return $this->isFavorite;
    }

    public function setIsFavorite(bool $isFavorite): self
    {
        $this->isFavorite = $isFavorite;

        return $this;
    }

    public function getLastConsulted(): ?\DateTimeInterface
    {
        return $this->lastConsulted;
    }

    public function setLastConsulted(?\DateTimeInterface $lastConsulted): self
    {
        $this->lastConsulted = $lastConsulted;

        return $this;
    }
}
