<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\BookRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use JetBrains\PhpStorm\Pure;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: BookRepository::class)]
#[Gedmo\SoftDeleteable(fieldName: 'deletedAt', timeAware: false, hardDelete: true)]
#[ApiResource]
class Book
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id = null;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups("book-user:read")]
    private ?string $title;

    #[ORM\Column(type: 'text', length: 255, nullable: true)]
    #[Groups("book-user:read")]
    private ?string $cover = null;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Groups("book-user:read")]
    private ?string $isbn = null;

    #[ORM\Column(type: 'text', nullable: true)]
    private ?string $resume = null;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Groups("book-user:read")]
    private ?string $author = null;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Groups("book-user:read")]
    private ?string $publisher = null;

    #[ORM\Column(type: 'integer', nullable: true)]
    #[Groups("book-user:read")]
    private ?int $pageCount = null;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private string $apiItemId;

    #[ORM\OneToMany(mappedBy: 'book', targetEntity: Quote::class, cascade: ['remove'])]
    private Collection $quotes;

    #[Gedmo\Timestampable(on: 'create')]
    #[ORM\Column(name: 'created', type: Types::DATETIME_MUTABLE)]
    private \DateTime $created;

    #[ORM\Column(name: 'updated', type: Types::DATETIME_MUTABLE)]
    #[Gedmo\Timestampable]
    private \DateTime $updated;

    #[ORM\Column(name: 'deletedAt', type: Types::DATETIME_MUTABLE, nullable: true)]
    private \DateTime $deletedAt;

    #[ORM\OneToMany(mappedBy: 'book', targetEntity: BookUser::class, cascade: ['persist'])]
    private Collection $bookUsers;

    #[Pure] public function __construct()
    {
        $this->quotes = new ArrayCollection();
        $this->bookUsers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getCover(): ?string
    {
        return $this->cover;
    }

    public function setCover(?string $cover): self
    {
        $this->cover = $cover;

        return $this;
    }

    public function getIsbn(): ?string
    {
        return $this->isbn;
    }

    public function setIsbn(string $isbn): self
    {
        $this->isbn = $isbn;

        return $this;
    }

    public function getResume(): ?string
    {
        return $this->resume;
    }

    public function setResume(?string $resume): self
    {
        $this->resume = $resume;

        return $this;
    }

    public function getAuthor(): ?string
    {
        return $this->author;
    }

    public function setAuthor(?string $author): self
    {
        $this->author = $author;

        return $this;
    }

    public function getPublisher(): ?string
    {
        return $this->publisher;
    }

    public function setPublisher(?string $publisher): self
    {
        $this->publisher = $publisher;

        return $this;
    }

    public function getPageCount(): ?int
    {
        return $this->pageCount;
    }

    public function setPageCount(?int $pageCount): self
    {
        $this->pageCount = $pageCount;

        return $this;
    }

    public function getApiItemId(): string
    {
        return $this->apiItemId;
    }

    public function setApiItemId(string $apiItemId): self
    {
        $this->apiItemId = $apiItemId;

        return $this;
    }

    public function getQuotes(): Collection
    {
        return $this->quotes;
    }

    public function setQuotes(Collection $quotes): self
    {
        $this->quotes = $quotes;

        return $this;
    }

    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    public function setCreated(\DateTime $created): self
    {
        $this->created = $created;

        return $this;
    }

    public function getUpdated(): \DateTime
    {
        return $this->updated;
    }

    public function setUpdated(\DateTime $updated): self
    {
        $this->updated = $updated;

        return $this;
    }

    public function getDeletedAt(): \DateTime
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(\DateTime $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    public function getBookUsers(): Collection
    {
        return $this->bookUsers;
    }

    public function addUser(User $user): self
    {
        $bookUser = new BookUser();
        $bookUser->setUser($user)
            ->setBook($this)
        ;
        $this->addBookUser($bookUser);

        return $this;
    }

    public function addBookUser(BookUser $bookUser): self
    {
        if (!$this->bookUsers->contains($bookUser)) {
            $this->bookUsers[] = $bookUser;
            $bookUser->setBook($this);
        }

        return $this;
    }

    public function removeBookUser(BookUser $bookUser): self
    {
        if ($this->bookUsers->removeElement($bookUser)) {
            if ($bookUser->getBook() === $this) {
                $bookUser->setBook(null);
            }
        }

        return $this;
    }
}
