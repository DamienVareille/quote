<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use JetBrains\PhpStorm\Pure;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @UniqueEntity(fields={"mail"}, message="There is already an account with this mail")
 */
#[ORM\Entity(repositoryClass: UserRepository::class)]
#[ORM\Table(name: '`user`')]
#[Gedmo\SoftDeleteable(fieldName: 'deletedAt', timeAware: false, hardDelete: true)]
#[ApiResource]
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    public const ROLE_USER = 'ROLE_USER';
    public const ROLE_ADMIN = 'ROLE_ADMIN';

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private int $id;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups("book-user:read")]
    private string $lastname;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups("book-user:read")]
    private string $firstname;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups("book-user:read")]
    private string $username;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups("book-user:read")]
    private string $mail;

    #[ORM\Column(type: 'string', length: 255)]
    private string $password;

    #[ORM\Column(type: 'array', nullable: true)]
    private array $roles = [];

    #[ORM\Column(type: 'boolean')]
    private bool $isVerified = false;

    #[ORM\OneToMany(mappedBy: 'author', targetEntity: Quote::class, orphanRemoval: true)]
    private Collection $quotes;

    #[ORM\ManyToMany(targetEntity: 'App\Entity\Quote', mappedBy: 'usersWhoLiked')]
    private Collection $likedQuotes;

    #[Gedmo\Timestampable(on: 'create')]
    #[ORM\Column(name: 'created', type: Types::DATETIME_MUTABLE)]
    private \DateTime $created;

    #[ORM\Column(name: 'updated', type: Types::DATETIME_MUTABLE)]
    #[Gedmo\Timestampable]
    private \DateTime $updated;

    #[ORM\Column(name: 'deletedAt', type: Types::DATETIME_MUTABLE, nullable: true)]
    private \DateTime $deletedAt;

    #[ORM\OneToMany(mappedBy: 'user', targetEntity: BookUser::class)]
    private Collection $bookUsers;

    #[ORM\Column(type: 'boolean')]
    private bool $shareQuotes = true;

    #[ORM\Column(type: 'datetime', nullable: true)]
    private ?\DateTimeInterface $blockedAt;

    #[Pure] public function __construct()
    {
        $this->quotes = new ArrayCollection();
        $this->likedQuotes = new ArrayCollection();
        $this->bookUsers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getMail(): ?string
    {
        return $this->mail;
    }

    public function setMail(string $mail): self
    {
        $this->mail = $mail;

        return $this;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function addRole(string $role): self
    {
        $this->roles[] = $role;

        return $this;
    }

    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getUserIdentifier(): string
    {
        return $this->mail;
    }

    public function setRoles(?array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    public function isVerified(): bool
    {
        return $this->isVerified;
    }

    public function setIsVerified(bool $isVerified): self
    {
        $this->isVerified = $isVerified;

        return $this;
    }

    public function getUsername(): string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getQuotes(): Collection
    {
        return $this->quotes;
    }

    public function addQuote(Quote $quote): self
    {
        if (!$this->quotes->contains($quote)) {
            $this->quotes[] = $quote;
            $quote->setAuthor($this);
        }

        return $this;
    }

    public function removeQuote(Quote $quote): self
    {
        if ($this->quotes->removeElement($quote)) {
            // set the owning side to null (unless already changed)
            if ($quote->getAuthor() === $this) {
                $quote->setAuthor(null);
            }
        }

        return $this;
    }

    public function getLikedQuotes(): Collection
    {
        return $this->likedQuotes;
    }

    public function setLikedQuotes(Collection $likedQuotes): self
    {
        $this->likedQuotes = $likedQuotes;

        return $this;
    }

    public function addLikedQuote(Quote $quote): self
    {
        if (!$this->likedQuotes->contains($quote)) {
            $this->likedQuotes->add($quote);
        }

        return $this;
    }

    public function removeLikedQuote(Quote $quote): self
    {
        if ($this->likedQuotes->contains($quote)) {
            $this->likedQuotes->removeElement($quote);
        }

        return $this;
    }

    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    public function setCreated(\DateTime $created): self
    {
        $this->created = $created;

        return $this;
    }

    public function getUpdated(): \DateTime
    {
        return $this->updated;
    }

    public function setUpdated(\DateTime $updated): self
    {
        $this->updated = $updated;

        return $this;
    }

    public function getDeletedAt(): \DateTime
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(\DateTime $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    public function getBookUsers(): Collection
    {
        return $this->bookUsers;
    }

    public function setBookUsers(Collection $bookUsers)
    {
        $this->bookUsers = $bookUsers;
    }

    public function addBookUser(BookUser $bookUser): self
    {
        if (!$this->bookUsers->contains($bookUser)) {
            $this->bookUsers[] = $bookUser;
            $bookUser->setUser($this);
        }

        return $this;
    }

    public function removeBookUser(BookUser $bookUser): self
    {
        if ($this->bookUsers->removeElement($bookUser)) {
            if ($bookUser->getUser() === $this) {
                $bookUser->setUser(null);
            }
        }

        return $this;
    }

    public function getCurrentReadings(): array
    {
        $readings = [];

        /** @var BookUser $bookUser */
        foreach ($this->bookUsers as $bookUser) {
            if ($bookUser->isFavorite()) {
                $readings[] = $bookUser->getBook();
            }
        }
        return $readings;
    }

    public function getBooks(): array
    {
        $books = [];
        /** @var BookUser $bookUser */
        foreach ($this->bookUsers as $bookUser) {
            $books[] = $bookUser->getBook();
        }
        return $books;
    }

    public function shareQuotes(): bool
    {
        return $this->shareQuotes;
    }

    public function setShareQuotes(bool $shareQuotes): void
    {
        $this->shareQuotes = $shareQuotes;
    }

    public function getBlockedAt(): ?\DateTimeInterface
    {
        return $this->blockedAt;
    }

    public function setBlockedAt(?\DateTimeInterface $blockedAt): self
    {
        $this->blockedAt = $blockedAt;

        return $this;
    }

    public function isBlocked(): bool
    {
        return null !== $this->getBlockedAt() && new \DateTime('now') > $this->getBlockedAt();
    }
}
