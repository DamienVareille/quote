<?php

namespace App\Assembler;

use Google\Service\Books\Volume;

interface AssemblerInterface
{
    public function writeObjectFromGoogleBookVolume(Volume $volume);
}
