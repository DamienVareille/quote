<?php

namespace App\Assembler;

use App\Entity\Book;
use Google\Service\Books\Volume;

class BookAssembler implements AssemblerInterface
{
    public function writeObjectFromGoogleBookVolume(Volume $volume): Book
    {
        $book = new Book();

        if (isset($volume->id)) {
            $book->setApiItemId($volume->id);
        }

        if (isset($volume->volumeInfo->authors)) {
            $author = implode(", ", $volume->volumeInfo->authors);
            $book->setAuthor($author);
        }

        if (isset($volume->volumeInfo->title)) {
            $book->setTitle($volume->volumeInfo->title);
        }

        if (isset($volume->volumeInfo['imageLinks']['thumbnail'])) {
            $book->setCover($volume->volumeInfo['imageLinks']['thumbnail']);
        }

        if (isset($volume->volumeInfo['pageCount'])) {
            $book->setPageCount($volume->volumeInfo['pageCount']);
        }

        if (isset($volume->volumeInfo['description'])) {
            $book->setResume($volume->volumeInfo['description']);
        }

        if (isset($volume->volumeInfo['publisher'])) {
            $book->setPublisher($volume->volumeInfo['publisher']);
        }

        return $book;
    }
}
