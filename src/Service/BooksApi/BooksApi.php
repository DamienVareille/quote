<?php

namespace App\Service\BooksApi;

use App\Entity\Book;

class BooksApi
{
    private BooksApiInterface $currentApi;

    public function __construct(GoogleBooksService $currentApi)
    {
        $this->currentApi = $currentApi;
    }

    public function getByQuery(string $query): array
    {
        return $this->currentApi->getByQuery($query);
    }

    public function getById(string $id): Book
    {
        return $this->currentApi->getById($id);
    }
}
