<?php

namespace App\Service\BooksApi;

use App\Entity\Book;

interface BooksApiInterface
{
    public function getByQuery(string $query): array;
    public function getById(string $id): Book;
}
