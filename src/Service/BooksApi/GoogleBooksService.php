<?php

namespace App\Service\BooksApi;

use App\Assembler\BookAssembler;
use App\Entity\Book;
use Google;

final class GoogleBooksService implements BooksApiInterface
{
    private string $apiKey = '';
    private BookAssembler $bookAssembler;

    public function __construct(string $apiKey, BookAssembler $bookAssembler)
    {
        $this->apiKey = $apiKey;
        $this->bookAssembler = $bookAssembler;
    }

    public function getByQuery(string $query): array
    {
        $client = new Google\Client();
        $client->setApplicationName("Quote");
        $client->setDeveloperKey($this->apiKey);

        $service = new Google\Service\Books($client);
        $results = $service->volumes->listVolumes($query, ['printType' => 'books']);

        return $this->googleBooksVolumesToBooks($results->getItems());
    }

    public function getById(string $id): Book
    {
        $client = new Google\Client();
        $client->setApplicationName("Quote");
        $client->setDeveloperKey($this->apiKey);

        $service = new Google\Service\Books($client);
        $result = $service->volumes->get($id);

        return $this->googleBooksVolumesToBooks([$result])[0];
    }

    private function googleBooksVolumesToBooks(array $volumes): array
    {
        $books = [];
        foreach ($volumes as $volume) {
            $books[] = $this->bookAssembler->writeObjectFromGoogleBookVolume($volume);
        }

        return $books;
    }
}
