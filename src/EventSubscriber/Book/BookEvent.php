<?php

namespace App\EventSubscriber\Book;

use Doctrine\Bundle\DoctrineBundle\EventSubscriber\EventSubscriberInterface;
use Doctrine\ORM\Events;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Psr\Cache\InvalidArgumentException;
use Psr\Log\LoggerInterface;
use Symfony\Contracts\Cache\TagAwareCacheInterface;

class BookEvent implements EventSubscriberInterface
{
    private TagAwareCacheInterface $pool;
    private LoggerInterface $logger;

    public function __construct(TagAwareCacheInterface $pool, LoggerInterface $logger)
    {
        $this->pool = $pool;
        $this->logger = $logger;
    }

    public function getSubscribedEvents(): array
    {
        return [
            Events::postRemove,
        ];
    }

    public function postRemove(): void
    {
        try {
            $this->pool->delete('favorites_books_cache');
        } catch (InvalidArgumentException $e) {
            $this->logger->warning('Cannot delete cache named favorites_books_cache');
        }
    }
}
