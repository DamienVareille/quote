<?php

namespace App\Controller;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\CurrentUser;

#[Route('/settings')]
class UserController extends AbstractController
{
    #[Route('/parameters', name: 'parameters')]
    public function parameters(): Response
    {
        return $this->render('parameters/settings.html.twig');
    }

    #[Route('/change-share-quotes', name: 'change_share_quotes')]
    public function changeShareQuotes(#[CurrentUser] User $user, EntityManagerInterface $em): Response
    {
        $user->setShareQuotes(!$user->shareQuotes());
        $em->persist($user);
        $em->flush();

        return $this->json(null, Response::HTTP_OK);
    }

    #[Route('/share-quotes', name: 'share_quotes')]
    public function shareQuotes(#[CurrentUser] User $user): Response
    {
        $data = ['shareQuotes' => $user->shareQuotes()];

        return $this->json(json_encode($data), Response::HTTP_OK);
    }
}
