<?php

namespace App\Controller;

use App\Entity\Book;
use App\Entity\Quote;
use App\Entity\User;
use App\Form\QuoteType;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\CurrentUser;

#[Route('/quote')]
#[IsGranted('ROLE_USER')]
class QuoteController extends AbstractController
{
    #[Route('/list/{id}', name: 'list_quote', methods: ['GET'])]
    #[ParamConverter('book', class: 'App\Entity\Book')]
    public function list(Book $book, #[CurrentUser] User $user, EntityManagerInterface $em): Response
    {
        $this->denyAccessUnlessGranted('view', $book);
        $quotes = $em->getRepository(Quote::class)->findBy(['author' => $user, 'book' => $book], ['created' => 'DESC']);

        return $this->render('quote/_list.html.twig', [
            'quotes' => $quotes,
            'book' => $book,
            'user' => $user
        ]);
    }

    #[Route('/list-others/{id}', name: 'list_other_quote', methods: ['GET'])]
    #[ParamConverter('book', class: 'App\Entity\Book')]
    public function listOther(Book $book, #[CurrentUser] User $user, EntityManagerInterface $em): Response
    {
        $this->denyAccessUnlessGranted('view', $book);

        $quotes = [];
        if ($user->shareQuotes()) {
            $quotes = $em->getRepository(Quote::class)->findWhereNotPersonnal($user, $book);
        }

        return $this->render('quote/_other_list.html.twig', [
            'quotes' => $quotes,
            'book' => $book,
            'user' => $user
        ]);
    }

    #[Route('/like/{id}', name: 'like_quote', methods: ['PUT'])]
    #[ParamConverter('quote', class: 'App\Entity\Quote')]
    public function like(Quote $quote, #[CurrentUser] User $user, EntityManagerInterface $em): Response
    {
        if ($user->getLikedQuotes()->contains($quote)) {
            $quote->removeUserWhoLiked($user);
        } else {
            $quote->addUserWhoLiked($user);
        }

        $em->persist($quote);
        $em->flush();

        return new Response(null, Response::HTTP_OK);
    }

    #[Route('/delete/{id}', name: 'delete_quote', methods: ['DELETE'])]
    #[ParamConverter('quote', class: 'App\Entity\Quote')]
    public function delete(Quote $quote, EntityManagerInterface $em): Response
    {
        $this->denyAccessUnlessGranted('edit', $quote);
        $em->remove($quote);
        $em->flush();

        return $this->json(null, Response::HTTP_NO_CONTENT);
    }
}
