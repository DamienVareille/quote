<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\BookRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

class HomeController extends AbstractController
{
    private Security $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }
    #[Route('/', name: 'home', methods: ['GET'])]
    #[IsGranted('ROLE_USER')]
    public function index(Request $request, BookRepository $bookRepository): Response
    {
        /** @var User $user */
        $user = $this->security->getUser();
        $page = $request->query->get('page') ?? 1;

        $books = $bookRepository->findOrderByCitationsCount($user, $page);

        if ($request->query->get('partial')) {
            return $this->render('home/_books_list.html.twig', [
                'books' => $books
            ]);
        }

        return $this->render('home/index.html.twig', [
            'books' => $books
        ]);
    }
}
