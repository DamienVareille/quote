<?php

namespace App\Controller;

use App\Entity\Book;
use App\Entity\BookUser;
use App\Entity\Quote;
use App\Entity\User;
use App\Form\QuoteType;
use App\Repository\BookUserRepository;
use App\Service\BooksApi\BooksApi;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Cache\InvalidArgumentException;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\CurrentUser;
use Symfony\Component\Translation\Exception\NotFoundResourceException;
use Symfony\Contracts\Cache\TagAwareCacheInterface;

#[Route('/book')]
#[IsGranted('ROLE_USER')]
class BookController extends AbstractController
{
    private LoggerInterface $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    #[Route('/find', name: 'book_get')]
    public function get(Request $request, #[CurrentUser] User $user, EntityManagerInterface $em): Response
    {
        $apiId = $request->query->get('apiItemId');

        $existingBook = $em->getRepository(Book::class)->findOneByUserAndApiItemId($user, $apiId);

        return $this->json([
            'inLibrary' => isset($existingBook)
        ], Response::HTTP_OK);
    }

    #[Route('/resume', name: 'book_resume')]
    public function bookResume(Request $request, BooksApi $booksApi): Response
    {
        $id = $request->query->get('id');

        $book = $booksApi->getById($id);

        return $this->render('book/_resume.html.twig', [
            'book' => $book
        ]);
    }

    #[Route('/search', name: 'search_book', methods: ['GET'])]
    public function search(Request $request, BooksApi $booksApi): Response
    {
        $query = $request->query->get('q');

        $books = $booksApi->getByQuery($query);

        $covers = [];
        foreach ($books as $book) {
            $covers[] = $book->getCover();
        }

        // Display books with cover first
        array_multisort($covers, SORT_DESC, $books);

        return $this->render('book/_searchPreview.html.twig', [
            'books' => $books
        ]);
    }

    #[Route('/view/{id}', name: 'view_book')]
    #[ParamConverter('book', class: 'App\Entity\Book')]
    public function view(Book $book, Request $request, #[CurrentUser] User $user, EntityManagerInterface $em, BookUserRepository $bookUserRepository): Response
    {
        if (!\in_array($book, $user->getBooks())) {
            throw $this->createNotFoundException();
        }

        $bookUser = $bookUserRepository->findOneBy(['user' => $user, 'book' => $book]);

        $bookUser?->setLastConsulted(new \DateTime('now'));
        $em->persist($bookUser);
        $em->flush();

        $quote = new Quote();

        $form = $this->createForm(QuoteType::class, $quote);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid() && $request->getMethod() === Request::METHOD_POST) {
            $quote
                ->setBook($book)
                ->setAuthor($user)
            ;
            $em->persist($quote);
            $em->flush();

            $this->redirectToRoute('view_book', ['id' => $book->getId()]);
        }

        $otherQuotes = [];
        if ($user->shareQuotes()) {
            $otherQuotes = $em->getRepository(Quote::class)->findWhereNotPersonnal($user, $book);
        }

        return $this->renderForm('book/view.html.twig', [
            'book' => $book,
            'personnalQuotes' => $em->getRepository(Quote::class)->findBy(['author' => $user, 'book' => $book], ['created' => 'DESC']),
            'otherQuotes' => $otherQuotes,
            'form' => $form,
            'user' => $user
        ]);
    }

    #[Route('/save', name: 'save_book', methods: ['POST'])]
    public function create(#[CurrentUser] ?User $user, Request $request, BooksApi $booksApi, EntityManagerInterface $em): Response
    {
        $bookId = $request->request->get('book_id');

        $existingBook = $em->getRepository(Book::class)->findOneByUserAndApiItemId($user, $bookId);

        if (null !== $existingBook) {
            return $this->json('Book already in library.', Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $existingBook = $em->getRepository(Book::class)->findOneByApiItemId($bookId);
        $book = $booksApi->getById($bookId);

        if ($existingBook) {
            $book = $existingBook;
        }

        $book->addUser($user);
        $em->persist($book);
        $em->flush();

        return $this->json('Book created', Response::HTTP_CREATED);
    }

    #[Route('/delete/book-user/{id}', name: 'delete_book_user', methods: ['DELETE'])]
    #[ParamConverter('book', class: 'App\Entity\Book')]
    public function deleteBookUser(#[CurrentUser] User $user, Book $book, EntityManagerInterface $em, TagAwareCacheInterface $pool): Response
    {
        $this->denyAccessUnlessGranted('edit', $book);
        $bookUser = $em->getRepository(BookUser::class)->findOneBy(['user' => $user, 'book' => $book]);

        if (null === $bookUser) {
            return $this->json(null, Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        $em->remove($bookUser);
        $em->flush();

        try {
            $pool->delete('favorites_books_cache' . $user->getId());
        } catch (InvalidArgumentException $e) {
            $this->logger->error('User#' . $user->getId() . ' ' . $e->getMessage());
        }

        return $this->json('BookUser removed', Response::HTTP_NO_CONTENT);
    }

    #[Route('/add-to-current-readings/{id}', name: 'add_to_current_readings')]
    #[ParamConverter('book', class: 'App\Entity\Book')]
    public function addToCurrentReadings(#[CurrentUser] User $user, Book $book, EntityManagerInterface $em): Response
    {
        $this->denyAccessUnlessGranted('view', $book);
        /** @var BookUser $bookUser */
        $bookUser = $em->getRepository(BookUser::class)->findOneBy(['user' => $user, 'book' => $book]);

        if (null === $bookUser) {
            return $this->json(null, Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $bookUser->setIsFavorite(!$bookUser->isFavorite());

        $em->persist($bookUser);
        $em->flush();

        return $this->json(null, Response::HTTP_OK);
    }

    #[Route('/current-readings', name: 'get_current_readings', methods: ['GET'])]
    public function getCurrentReadings(Request $request, #[CurrentUser] User $user, EntityManagerInterface $em): Response
    {
        $template = $request->query->get('partial') ? 'home/_books_list.html.twig' : 'current_readings/list_current_readings.html.twig';
        $bookUsers = $em->getRepository(BookUser::class)->findBy(['user' => $user, 'isFavorite' => true]);
        $books = [];

        /** @var BookUser $bookUser */
        foreach ($bookUsers as $bookUser) {
            $books[] = $bookUser->getBook();
        }

        return $this->render($template, [
            'books' => $books
        ]);
    }
}
