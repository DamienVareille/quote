<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\ForgottenPasswordType;
use App\Form\ResetPasswordType;
use App\Repository\UserRepository;
use App\Security\EmailVerifier;
use App\Security\PasswordResetter;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Contracts\Translation\TranslatorInterface;
use SymfonyCasts\Bundle\VerifyEmail\Exception\VerifyEmailExceptionInterface;

#[Route('/public')]
class SecurityController extends AbstractController
{
    private TranslatorInterface $translator;
    private EmailVerifier $emailVerifier;

    public function __construct(TranslatorInterface $translator, EmailVerifier $emailVerifier)
    {
        $this->translator = $translator;
        $this->emailVerifier = $emailVerifier;
    }

    #[Route('/login', name: 'app_login')]
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }

    #[Route('/logout', name: 'app_logout')]
    public function logout(): void
    {
        throw new \LogicException('Should not be reached.');
    }

    /**
     * @throws TransportExceptionInterface
     */
    #[Route('/forgotten_password', name: 'forgotten_password')]
    public function forgottenPassword(Request $request, PasswordResetter $passwordResetter, UserRepository $userRepository): Response
    {
        $user = new User();
        $form = $this->createForm(ForgottenPasswordType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $userRepository->findOneBy(['mail' => $user->getMail(), 'isVerified' => true]);

            if (null !== $user) {
                $passwordResetter->sendPasswordResetEmail(
                    'reset_password',
                    $user,
                    (new TemplatedEmail())
                        ->from(new Address($this->getParameter('app.admin_email'), 'Quote'))
                        ->to($user->getMail())
                        ->subject($this->translator->trans('Reset your password'))
                        ->htmlTemplate('security/reset_password_email.html.twig')
                );
            }

            $this->addFlash('success', 'We sent you an email to reset your password (if this email is link to an activated account).');
        }

        return $this->renderForm('security/forgotten_password.html.twig', [
            'form' => $form
        ]);
    }

    #[Route('/reset_password', name: 'reset_password')]
    public function resetPassword(Request $request, UserRepository $userRepository, EntityManagerInterface $em, UserPasswordHasherInterface $passwordHasher): Response
    {
        $userId = $request->query->get('id');
        $user = $userRepository->find($userId);

        try {
            $this->emailVerifier->handleEmailConfirmation($request, $user);
        } catch (VerifyEmailExceptionInterface $exception) {
            $this->addFlash('danger', $exception->getReason());

            return $this->redirectToRoute('app_login');
        }

        $form = $this->createForm(ResetPasswordType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user->setPassword(
                $passwordHasher->hashPassword(
                    $user,
                    $form->get('password')->getData()
                )
            );
            $em->persist($user);
            $em->flush();

            $this->addFlash('success', 'Your password has been reset. You can now log in.');

            return $this->redirectToRoute('app_login');
        }

        return $this->renderForm('security/reset_password.html.twig', [
            'form' => $form
        ]);
    }
}
