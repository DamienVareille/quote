<?php

namespace App\Voter;

use App\Entity\Book;
use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\CacheableVoterInterface;

class BookVoter implements CacheableVoterInterface
{
    public const VIEW = 'view';
    public const EDIT = 'edit';

    public function supportsAttribute(string $attribute): bool
    {
        if (!\in_array($attribute, [self::VIEW, self::EDIT])) {
            return false;
        }

        return true;
    }

    public function supportsType(string $subjectType): bool
    {
        if (Book::class !== $subjectType) {
            return false;
        }

        return true;
    }

    public function vote(TokenInterface $token, mixed $subject, array $attributes): bool|int
    {
        $user = $token->getUser();

        if (!$user instanceof User) {
            return false;
        }

        /** @var Book $book */
        $book = $subject;

        switch ($attributes) {
            case [self::EDIT]:
            case [self::VIEW]:
                return $this->canView($book, $user);
        }

        throw new \LogicException('This code should not be reached!');
    }

    private function canView(Book $book, User $user): int
    {
        return \in_array($book, $user->getBooks()) ? self::ACCESS_GRANTED : self::ACCESS_DENIED;
    }
}
