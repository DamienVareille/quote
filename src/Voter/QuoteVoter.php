<?php

namespace App\Voter;

use App\Entity\Quote;
use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\CacheableVoterInterface;

class QuoteVoter implements CacheableVoterInterface
{
    public const VIEW = 'view';
    public const EDIT = 'edit';

    public function supportsAttribute(string $attribute): bool
    {
        if (!\in_array($attribute, [self::VIEW, self::EDIT])) {
            return false;
        }

        return true;
    }

    public function supportsType(string $subjectType): bool
    {
        if (Quote::class !== $subjectType) {
            return false;
        }

        return true;
    }

    public function vote(TokenInterface $token, mixed $subject, array $attributes): bool|int
    {
        $user = $token->getUser();

        if (!$user instanceof User) {
            return false;
        }

        /** @var Quote $quote */
        $quote = $subject;

        switch ($attributes) {
            case [self::EDIT]:
            case [self::VIEW]:
                return $this->canView($quote, $user);
        }

        throw new \LogicException('This code should not be reached!');
    }

    private function canView(Quote $quote, User $user): int
    {
        return $user->getQuotes()->contains($quote) ? self::ACCESS_GRANTED : self::ACCESS_DENIED;
    }
}
