<?php

namespace App\Repository;

use App\Entity\BookUser;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method BookUser|null find($id, $lockMode = null, $lockVersion = null)
 * @method BookUser|null findOneBy(array $criteria, array $orderBy = null)
 * @method BookUser[]    findAll()
 * @method BookUser[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BookUserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BookUser::class);
    }

    // /**
    //  * @return BookUser[] Returns an array of BookUser objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BookUser
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
