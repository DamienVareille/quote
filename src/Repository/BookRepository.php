<?php

namespace App\Repository;

use App\Entity\Book;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Book|null find($id, $lockMode = null, $lockVersion = null)
 * @method Book|null findOneBy(array $criteria, array $orderBy = null)
 * @method Book[]    findAll()
 * @method Book[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BookRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Book::class);
    }

    public function findOrderByCitationsCount(User $user, int $page): array
    {
        $query = $this->createQueryBuilder('b')
            ->join('b.bookUsers', 'bu')
            ->join('bu.user', 'u')
            ->leftJoin('b.quotes', 'q')
            ->where('u.id = :user_id')
            ->setParameter('user_id', $user->getId())
            ->groupBy('b.id, b.title, b.cover, b.isbn, b.resume, b.author, b.publisher, b.pageCount, b.apiItemId, bu.created')
            ->orderBy('bu.created', 'DESC')
            ->getQuery()
        ;

        $paginator = new Paginator($query);

        // now get one page's items:
        $paginator
            ->getQuery()
            ->setFirstResult(10 * ($page-1)) // set the offset
            ->setMaxResults(10); // set the limit

        return $paginator->getQuery()->getResult();
    }

    /**
     * @throws NonUniqueResultException
     */
    public function findOneByUserAndApiItemId(User $user, string $apiId): ?Book
    {
        return $this->createQueryBuilder('b')
            ->join('b.bookUsers', 'bu')
            ->join('bu.user', 'u')
            ->where('u.id = :user_id')
            ->andWhere('b.apiItemId = :api_id')
            ->setParameter('user_id', $user->getId())
            ->setParameter('api_id', $apiId)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    /**
     * @throws NonUniqueResultException
     */
    public function findOneByApiItemId(string $apiId): ?Book
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.apiItemId = :api_id')
            ->setParameter('api_id', $apiId)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    public function findLastConsultedBooks(User $user, int $limit = 1): array
    {
        return $this->createQueryBuilder('b')
            ->join('b.bookUsers', 'bu')
            ->join('bu.user', 'u')
            ->where('u.id = :user_id')
            ->orderBy('bu.lastConsulted', 'DESC')
            ->setMaxResults($limit)
            ->setParameter('user_id', $user->getId())
            ->getQuery()
            ->getResult()
            ;
    }
}
