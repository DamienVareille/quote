<?php

namespace App\Security;

use App\Entity\User;
use App\Repository\UserRepository;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Http\Authenticator\AbstractLoginFormAuthenticator;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\CsrfTokenBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\RememberMeBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Credentials\PasswordCredentials;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;
use Symfony\Component\Security\Http\Util\TargetPathTrait;
use Symfony\Contracts\Translation\TranslatorInterface;

class LoginFormAuthenticator extends AbstractLoginFormAuthenticator
{
    use TargetPathTrait;

    public const LOGIN_ROUTE = 'app_login';

    private UrlGeneratorInterface $urlGenerator;
    private UserRepository $userRepository;
    private EmailVerifier $emailVerifier;
    private TranslatorInterface $translator;
    private ParameterBagInterface $parameterBag;
    private LoggerInterface $logger;

    public function __construct(UrlGeneratorInterface $urlGenerator, UserRepository $userRepository, EmailVerifier $emailVerifier, TranslatorInterface $translator, ParameterBagInterface $parameterBag, LoggerInterface $logger)
    {
        $this->urlGenerator = $urlGenerator;
        $this->userRepository = $userRepository;
        $this->emailVerifier = $emailVerifier;
        $this->translator = $translator;
        $this->parameterBag = $parameterBag;
        $this->logger = $logger;
    }

    public function authenticate(Request $request): Passport
    {
        $mail = $request->request->get('mail', '');

        $request->getSession()->set(Security::LAST_USERNAME, $mail);

        return new Passport(
            new UserBadge($mail, function ($userIdentifier) {
                $user = $this->userRepository->findOneBy(['mail' => $userIdentifier]);

                if (null === $user) {
                    throw new BadCredentialsException();
                }

                if (!$user->isVerified()) {
                    $this->sendConfirmationEmail($user);
                    throw new CustomUserMessageAuthenticationException('The user is not verified. Check your emails.');
                }

                if ($user->isBlocked()) {
                    throw new CustomUserMessageAuthenticationException('Your account has been blocked. Retry later or contact us.');
                }

                return $user;
            }),
            new PasswordCredentials($request->request->get('password', '')),
            [
                new CsrfTokenBadge('authenticate', $request->request->get('_csrf_token')),
                (new RememberMeBadge())->enable(),
            ]
        );
    }

    /**
     * @throws Exception
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $firewallName): ?Response
    {
        if ($targetPath = $this->getTargetPath($request->getSession(), $firewallName)) {
            $this->logger->info('User ' . $token->getUser()->getUserIdentifier() . ' is logged in.');
            return new RedirectResponse($targetPath);
        }

        return new RedirectResponse($this->urlGenerator->generate('app_login'));
    }

    protected function getLoginUrl(Request $request): string
    {
        return $this->urlGenerator->generate(self::LOGIN_ROUTE);
    }

    /**
     * @throws TransportExceptionInterface
     */
    private function sendConfirmationEmail(User $user): void
    {
        $this->logger->info('Send confirmation email for User# ' . $user->getId());
        $this->emailVerifier->sendEmailConfirmation(
            'app_verify_email',
            $user,
            (new TemplatedEmail())
                ->from(new Address($this->parameterBag->get('app.admin_email'), 'Quote'))
                ->to($user->getMail())
                ->subject($this->translator->trans('Please confirm your email'))
                ->htmlTemplate('registration/confirmation_email.html.twig')
        );
    }
}
