<?php

namespace App\Security;

use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use SymfonyCasts\Bundle\VerifyEmail\Exception\VerifyEmailExceptionInterface;

class PasswordResetter extends EmailVerifier
{
    /**
     * @throws TransportExceptionInterface
     */
    public function sendPasswordResetEmail(string $passwordResetRouteName, UserInterface $user, TemplatedEmail $email): void
    {
        $this->sendEmailConfirmation($passwordResetRouteName, $user, $email);
    }

    /**
     * @throws VerifyEmailExceptionInterface
     */
    public function handleEmailConfirmation(Request $request, UserInterface $user): void
    {
        $this->verifyEmailHelper->validateEmailConfirmation($request->getUri(), $user->getId(), $user->getMail());
    }
}
