import {Controller} from "@hotwired/stimulus";
import {useDispatch} from "stimulus-use";
import $ from "jquery";

export default class extends Controller {
    static targets = ['content'];
    static values = {
        url : String,
        paginate: Boolean
    }

    pageCount = 2; // on refresh, we add page 2, then 3, and so on
    blockBooksFetching = false; // Block infinite scroll to limit over fetching

    connect() {
        if (this.paginateValue) {
            useDispatch(this);

            // Infinite scroll
            $(window).scroll(() => {
                if (!this.blockBooksFetching) {
                    if($(window).scrollTop() + $(window).height() === $(document).height()) {
                        $.ajax({
                            url: this.urlValue,
                            data: { page: this.pageCount }
                        }).then((r) => {
                            const booksCount = r.getElementById('booksCount').value;
                            if (0 === booksCount) {
                                this.blockBooksFetching = true;
                                return;
                            }
                            this.element.innerHTML += r;
                            ++this.pageCount;
                        });
                    }
                }

            });
        }
    }

    async refreshContent() {
        this.pageCount = 2;
        this.blockBooksFetching = false;
        const target = this.hasContentTarget ? this.contentTarget : this.element;
        const response = await fetch(this.urlValue);

        target.innerHTML = await response.text();
    }
}