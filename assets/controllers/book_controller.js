import {Controller} from "@hotwired/stimulus";

export default class extends Controller {

    static values = {
        url: String
    }

    onBookClicked() {
        window.location = this.urlValue;
    }
}