import { Controller } from "@hotwired/stimulus";
import {Modal} from "bootstrap";
import {useDispatch} from 'stimulus-use';
import $ from 'jquery';

export default class extends Controller {
    static targets = ['autocomplete', 'modal', 'modalBody', 'addButton'];

    static values = {
        url : String,
        bookIsInLibraryUrl: String,
        saveBookUrl : String,
        defaultModalBody: String,
        alreadyInLibrary: String,
        notInLibrary: String
    }

    modal = null;

    connect() {
        useDispatch(this);
    }

    autocomplete() {
        const booksItem = document.getElementsByClassName('book-search-item');

        Array.from(booksItem).forEach((book) => {
            book.addEventListener('click', (event) => {
                this.dispatch("openModal", {
                    bubbles: true,
                    detail: {id: event.currentTarget.id}
                })
            });
        });
    }

    openModal(event) {
        const bookId = event.detail.detail.id;
        $.ajax({
            url: this.bookIsInLibraryUrlValue,
            data: { apiItemId: bookId }
        }).then((r) => {
            if(true === r.inLibrary) {
                this.addButtonTarget.setAttribute('disabled', 'disabled');
                this.addButtonTarget.innerHTML = this.alreadyInLibraryValue;
            } else {
                this.addButtonTarget.removeAttribute('disabled');
                this.addButtonTarget.innerHTML = this.notInLibraryValue;
            }
        });

        $('.list-group').empty();
        $.ajax({
            url: this.urlValue,
            data: { id: bookId }
        }).then((r) => {
            this.modalBodyTarget.innerHTML = r;
        });
        this.modal = new Modal(this.modalTarget);
        this.modal.show();
    }

    submitModal() {
        const bookId = $('#book-id').val();
        $.ajax({
            url: this.saveBookUrlValue,
            method: 'post',
            data: { book_id: bookId }
        }).then(() => {
            this.modal.hide();
            this.dispatch('success', { bubbles: true });
        });
    }

    emptyModalOnClose() {
        this.modalBodyTarget.innerHTML = '';
    }
}