import {Controller} from "@hotwired/stimulus";
import $ from 'jquery'

export default class extends Controller {

    static targets = ['shareQuotesInput'];
    static values = {
        changeShareQuotesUrl: String,
        shareQuotesUrl: String
    }

    connect() {
        $.ajax({
            url: this.shareQuotesUrlValue
        }).then((r) => {
            const shareQuotes = JSON.parse(r).shareQuotes;
            $('#'+this.shareQuotesInputTarget.id).prop( "checked", shareQuotes );
        });

        $('#'+this.shareQuotesInputTarget.id).change(async () => {
            await fetch(this.changeShareQuotesUrlValue);
        });
    }
}