import {Controller} from "@hotwired/stimulus";
import {useDispatch} from "stimulus-use";

export default class extends Controller {

    static targets = ['likeButton', 'likeHeartIcon'];

    static values = {
        url: String
    }

    connect() {
        useDispatch(this);
    }

    async like(e) {
        e.stopImmediatePropagation();
        await fetch(this.urlValue, {method: 'PUT'}).then(() => {
            this.dispatch('action');
        })
    }
}