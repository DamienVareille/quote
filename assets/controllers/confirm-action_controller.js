import {Controller} from "@hotwired/stimulus";
import {useDispatch} from 'stimulus-use';
import Swal from "sweetalert2";
import $ from 'jquery'

export default class extends Controller {
    static values = {
        title: String,
        text: String,
        icon: String,
        confirmButtonText: String,
        cancelButtonText: String,
        method: String,
        url: String,
    }

    modal = null;

    connect() {
        useDispatch(this);
    }

    confirm(event) {
        event.stopImmediatePropagation();
        Swal.fire({
            title: this.titleValue || null,
            text: this.textValue || null,
            icon: this.iconValue || null,
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: this.confirmButtonTextValue || 'Yes',
            cancelButtonText: this.cancelButtonTextValue || 'Cancel',
            showLoaderOnConfirm: true,
            preConfirm: () => {
                return this.submit();
            }
        })
    }

    async submit() {
        $.ajax({
            url: this.urlValue,
            method: this.methodValue,
        }).then(() => {
            console.log(this.dispatch('async:confirmed'));
        });
    }
}