<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220122203859 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE book_user DROP CONSTRAINT book_user_pkey');
        $this->addSql('ALTER TABLE book_user ADD PRIMARY KEY (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP INDEX book_user_pkey');
        $this->addSql('ALTER TABLE book_user ALTER book_id SET NOT NULL');
        $this->addSql('ALTER TABLE book_user ALTER user_id SET NOT NULL');
        $this->addSql('ALTER TABLE book_user ADD PRIMARY KEY (book_id, user_id)');
    }
}
