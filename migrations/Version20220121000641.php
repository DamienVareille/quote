<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220121000641 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE liked_quotes (quote_id INT NOT NULL, user_id INT NOT NULL, PRIMARY KEY(quote_id, user_id))');
        $this->addSql('CREATE INDEX IDX_D3AC6A4EDB805178 ON liked_quotes (quote_id)');
        $this->addSql('CREATE INDEX IDX_D3AC6A4EA76ED395 ON liked_quotes (user_id)');
        $this->addSql('CREATE TABLE user_quote (user_id INT NOT NULL, quote_id INT NOT NULL, PRIMARY KEY(user_id, quote_id))');
        $this->addSql('CREATE INDEX IDX_89B330ACA76ED395 ON user_quote (user_id)');
        $this->addSql('CREATE INDEX IDX_89B330ACDB805178 ON user_quote (quote_id)');
        $this->addSql('ALTER TABLE liked_quotes ADD CONSTRAINT FK_D3AC6A4EDB805178 FOREIGN KEY (quote_id) REFERENCES quote (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE liked_quotes ADD CONSTRAINT FK_D3AC6A4EA76ED395 FOREIGN KEY (user_id) REFERENCES "user" (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_quote ADD CONSTRAINT FK_89B330ACA76ED395 FOREIGN KEY (user_id) REFERENCES "user" (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_quote ADD CONSTRAINT FK_89B330ACDB805178 FOREIGN KEY (quote_id) REFERENCES quote (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP TABLE liked_quotes');
        $this->addSql('DROP TABLE user_quote');
    }
}
