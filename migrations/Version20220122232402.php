<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220122232402 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE user_quote');
        $this->addSql('ALTER TABLE book_user ALTER book_id DROP NOT NULL');
        $this->addSql('ALTER TABLE book_user ALTER user_id DROP NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('CREATE TABLE user_quote (user_id INT NOT NULL, quote_id INT NOT NULL, PRIMARY KEY(user_id, quote_id))');
        $this->addSql('CREATE INDEX idx_89b330aca76ed395 ON user_quote (user_id)');
        $this->addSql('CREATE INDEX idx_89b330acdb805178 ON user_quote (quote_id)');
        $this->addSql('ALTER TABLE user_quote ADD CONSTRAINT fk_89b330aca76ed395 FOREIGN KEY (user_id) REFERENCES "user" (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_quote ADD CONSTRAINT fk_89b330acdb805178 FOREIGN KEY (quote_id) REFERENCES quote (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE book_user ALTER book_id SET NOT NULL');
        $this->addSql('ALTER TABLE book_user ALTER user_id SET NOT NULL');
    }
}
