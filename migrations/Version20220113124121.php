<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220113124121 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE book_user (book_id INT NOT NULL, user_id INT NOT NULL, PRIMARY KEY(book_id, user_id))');
        $this->addSql('CREATE INDEX IDX_940E9D41AD6C8EDB ON book_user (book_id)');
        $this->addSql('CREATE INDEX IDX_940E9D41A76ED395 ON book_user (user_id)');
        $this->addSql('ALTER TABLE book_user ADD CONSTRAINT FK_940E9D41AD6C8EDB FOREIGN KEY (book_id) REFERENCES book (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE book_user ADD CONSTRAINT FK_940E9D41A76ED395 FOREIGN KEY (user_id) REFERENCES "user" (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE book DROP CONSTRAINT fk_cbe5a331a76ed395');
        $this->addSql('DROP INDEX idx_cbe5a331a76ed395');
        $this->addSql('ALTER TABLE book DROP user_id');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE book_user');
        $this->addSql('ALTER TABLE book ADD user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE book ADD CONSTRAINT fk_cbe5a331a76ed395 FOREIGN KEY (user_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_cbe5a331a76ed395 ON book (user_id)');
    }
}
