<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211230222605 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE book ADD author VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE book ADD publisher VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE book ADD page_count INT DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE book DROP author');
        $this->addSql('ALTER TABLE book DROP publisher');
        $this->addSql('ALTER TABLE book DROP page_count');
    }
}
