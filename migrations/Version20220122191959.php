<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220122191959 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE book_user_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('ALTER TABLE book_user DROP CONSTRAINT FK_940E9D4116A2B381');
        $this->addSql('ALTER TABLE book_user DROP CONSTRAINT FK_940E9D41A76ED395');
        $this->addSql('ALTER TABLE book_user ADD id INT NOT NULL');
        $this->addSql('ALTER TABLE book_user ADD created TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL');
        $this->addSql('ALTER TABLE book_user ADD is_favorite BOOLEAN NOT NULL');
        $this->addSql('ALTER TABLE book_user ADD CONSTRAINT FK_940E9D4116A2B381 FOREIGN KEY (book_id) REFERENCES book (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE book_user ADD CONSTRAINT FK_940E9D41A76ED395 FOREIGN KEY (user_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE book_user_id_seq CASCADE');
        $this->addSql('CREATE INDEX idx_89b330aca76ed395 ON user_quote (user_id)');
        $this->addSql('CREATE INDEX idx_89b330acdb805178 ON user_quote (quote_id)');
        $this->addSql('ALTER TABLE book_user DROP CONSTRAINT fk_940e9d4116a2b381');
        $this->addSql('ALTER TABLE book_user DROP CONSTRAINT fk_940e9d41a76ed395');
        $this->addSql('DROP INDEX book_user_pkey');
        $this->addSql('ALTER TABLE book_user DROP id');
        $this->addSql('ALTER TABLE book_user DROP created');
        $this->addSql('ALTER TABLE book_user DROP is_favorite');
        $this->addSql('ALTER TABLE book_user ALTER book_id SET NOT NULL');
        $this->addSql('ALTER TABLE book_user ALTER user_id SET NOT NULL');
        $this->addSql('ALTER TABLE book_user ADD CONSTRAINT fk_940e9d4116a2b381 FOREIGN KEY (book_id) REFERENCES book (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE book_user ADD CONSTRAINT fk_940e9d41a76ed395 FOREIGN KEY (user_id) REFERENCES "user" (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE book_user ADD PRIMARY KEY (book_id, user_id)');
    }
}
